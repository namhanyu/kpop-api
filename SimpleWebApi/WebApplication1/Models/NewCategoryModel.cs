﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class NewCategoryModel
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public string information { get; set; }
        public string extra { get; set; }
    }
}
