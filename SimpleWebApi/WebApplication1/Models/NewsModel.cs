﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class NewsModel
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public string introduction { get; set; }
        public string information { get; set; }
        public string image { get; set; }
        public DateTime add_date { get; set; }
        public int comment { get; set; }
        public int id_acc { get; set; }
        public int category { get; set; }
        public string extra { get; set; }
    }
}
