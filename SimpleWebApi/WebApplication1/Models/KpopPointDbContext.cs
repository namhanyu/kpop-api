﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    //ref link: http://www.entityframeworktutorial.net/efcore/entity-framework-core.aspx

    public class KpopPointDbContext  : DbContext
    {
        public KpopPointDbContext(DbContextOptions<KpopPointDbContext> options)
           : base(options)
        {
        }

        public DbSet<AccountModel> Accounts { get; set; }
        public DbSet<BandModel> Bands { get; set; }
        public DbSet<CommentModel> Comments { get; set; }
        public DbSet<CompanyModel> Companys { get; set; }
        public DbSet<MvModel> Mvs { get; set; }
        public DbSet<MvTypeModel> MvTypes { get; set; }
        public DbSet<NationalModel> nationals { get; set; }
        public DbSet<NewsModel> News { get; set; }
        public DbSet<NewCategoryModel> NewCategorys { get; set; }
        public DbSet<SingerModel> Singers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountModel>().ToTable("account");
            modelBuilder.Entity<BandModel>().ToTable("band");
            modelBuilder.Entity<CommentModel>().ToTable("comment");
            modelBuilder.Entity<CompanyModel>().ToTable("company");
            modelBuilder.Entity<MvModel>().ToTable("mv");
            modelBuilder.Entity<MvTypeModel>().ToTable("mv_type");
            modelBuilder.Entity<SingerModel>().ToTable("singer");
        }
    }
}
