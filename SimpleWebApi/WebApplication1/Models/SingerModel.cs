﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class SingerModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string stage_name { get; set; }
        public int band { get; set; }
        public string information { get; set; }
        public string height { get; set; }
        public string weight { get; set; }
        public string eye_color { get; set; }
        public string hair_color { get; set; }
        public DateTime dateofbirth { get; set; }
        public int nationality { get; set; }
        public string hobby { get; set; }
        public int comment { get; set; }
        public DateTime debut_day { get; set; }
        public string image { get; set; }
        public string facebook { get; set; }
        public string twitter { get; set; }
        public string google { get; set; }
        public string youtube { get; set; }
        public string instagram { get; set; }
        public string extra { get; set; }
    }
}
