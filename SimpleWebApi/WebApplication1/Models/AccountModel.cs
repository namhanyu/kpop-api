﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class AccountModel
    {
        [Key]
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Int16 active { get; set; }
        public string avatar { get; set; }
        public string watch_list { get; set; }
        public string like_list { get; set; }
        public string save_list { get; set; }
        public string extra { get; set; }

        public void Update(AccountModel account)
        {
            if (account == null)
                return;

            id = account.id;
            username = account.username;
            password = account.password;
            active = account.active;
            avatar = account.avatar;
            watch_list = account.watch_list;
            like_list = account.like_list;
            save_list = account.save_list;
            extra = account.extra;
        }

    }
}
