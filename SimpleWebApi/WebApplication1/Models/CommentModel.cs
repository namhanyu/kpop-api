﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class CommentModel
    {
        [Key]
        public int id { get; set; }
        public int in_acc { get; set; }
        public string content_cmt { get; set; }
        public bool active { get; set; }
        public DateTime add_date { get; set; }
        public string extra { get; set; }
    }
}
