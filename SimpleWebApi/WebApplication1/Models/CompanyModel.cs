﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebApplication1.Models
{
    public class CompanyModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string founding { get; set; }
        public string image { get; set; }
        public string manager { get; set; }
        public string extra { get; set; }
    }
}
