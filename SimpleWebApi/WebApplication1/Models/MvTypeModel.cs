﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class MvTypeModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string information { get; set; }
        public string extra { get; set; }
    }
}
