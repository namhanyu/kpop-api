﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class BandModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public int company { get; set; }
        public string information { get; set; }
        public string image { get; set; }
        public DateTime debut_day { get; set; }
        public string introduction { get; set; }
        public int star { get; set; }
        public bool cover { get; set; }
        public string extra { get; set; }
    }
}
