﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class MvModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string information { get; set; }
        public int favourite { get; set; }
        public int rate { get; set; }
        public DateTime release_date { get; set; }
        public string link_youtube { get; set; }
        public string image { get; set; }
        public int star { get; set; }
        public int vote { get; set; }
        public int id_band { get; set; }
        public int id_singer { get; set; }
        public bool active { get; set; }
        public int type { get; set; }
        public string extra { get; set; }
    }
}
