﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApplication1.Models;
using WebApplication1.Models.ViewModel;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        public AccountController(KpopPointDbContext context,
            IConfiguration configuration) :
            base(context, configuration) { }

        /// <summary>
        /// Get all questions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<AccountModel>> GetAll()
        {
            var accounts = await DbContext.Accounts.ToListAsync();
            return accounts;
        }


        /// <summary>
        /// Get a question by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var question = await DbContext.Accounts.AsNoTracking().FirstOrDefaultAsync(q => q.id == id);

                if (question == null)
                {
                    return NotFound();
                }
                else
                {
                    return new ObjectResult(question);
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        ///// <summary>
        ///// POST api/question
        ///// </summary>
        ///// <param name="value"></param>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AccountModel item)
        {
            try
            {
                if (item == null)
                {
                    return BadRequest();
                }
                else
                {
                    DbContext.Accounts.Add(item);
                    await DbContext.SaveChangesAsync();

                    return new ObjectResult(item); // status 200 => OK
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        ///// <summary>
        ///// PUT api/question/id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="value"></param>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] AccountModel item)
        {
            try
            {
                if (item == null || item.id != id)
                {
                    return BadRequest();
                }
                else
                {
                    //var question = DbContext.Questions.AsNoTracking().FirstOrDefault(q => q.Id == id);
                    var account = DbContext.Accounts.FirstOrDefault(q => q.id == id);
                    if (account == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        account.Update(item);
                        // DbContext.Attach(question);
                        //  DbContext.Entry(question).State = EntityState.Modified;
                        await DbContext.SaveChangesAsync();

                        return new ObjectResult(account); // status 200 => OK
                    }
                }
            }
            catch
            {
                return BadRequest(); // status code = 400 
            }
        }

        ///// <summary>
        ///// DELETE api/question/id
        ///// </summary>
        ///// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if (id < 1)
                {
                    return BadRequest();
                }
                else
                {
                    var account = DbContext.Accounts.FirstOrDefault(q => q.id == id);
                    if (account == null)
                    {
                        return NotFound();
                    }

                    DbContext.Accounts.Remove(account);
                    await DbContext.SaveChangesAsync();
                }

                return new NoContentResult(); //status code = 204
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Check login
        /// </summary>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel item)
        {
            var accounts = await DbContext.Accounts.Where(x => x.username.Equals(item.username) && x.password.Equals(item.password)).ToListAsync();
            try
            {
                if(item == null)
                {
                    return BadRequest();
                }
                else
                {
                    if (accounts.Count <= 0)
                    {
                        return NotFound();
                    }
                    else
                    {
                        return new ObjectResult(accounts); // status 200 => OK
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
        }

    }
}









/* 
    return Ok(); // Http status code 200
    return Created(); // Http status code 201
    return NoContent(); // Http status code 204
    return BadRequest(); // Http status code 400
    return Unauthorized(); // Http status code 401
    return Forbid(); // Http status code 403
    return NotFound(); // Http status code 404
*/
